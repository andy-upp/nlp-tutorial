import re
import math
from collections import defaultdict



class build_graph(object):
    def __init__(self,city_coordination):
        self.city_coordination = city_coordination
        self.city_info = {}
        self.threshold = {}
    #用正则表达式从原始信息中提取城市和经纬度信息
    def get_city_info(self,city_coordination):
        city_location = {}
        for line in city_coordination.split("\n"):
            if line.startswith("//"): continue
            if line.strip() == "": continue

            city = re.findall("name:'(\w+)'", line)[0]
            x_y = re.findall("Coord:\[(\d+.\d+),\s(\d+.\d+)\]", line)[0]
            x_y = tuple(map(float, x_y))
            city_location[city] = x_y
        return city_location

    def geo_distance(self,origin, destination):
        """
        Calculate the Haversine distance.

        Parameters
        ----------
        origin : tuple of float
            (lat, long)
        destination : tuple of float
            (lat, long)

        Returns
        -------
        distance_in_km : float

        Examples
        --------
        >>> origin = (48.1372, 11.5756)  # Munich
        >>> destination = (52.5186, 13.4083)  # Berlin
        >>> round(distance(origin, destination), 1)
        504.2
        """
        lat1, lon1 = origin
        lat2, lon2 = destination
        radius = 6371  # km

        dlat = math.radians(lat2 - lat1)
        dlon = math.radians(lon2 - lon1)
        a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
             math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
             math.sin(dlon / 2) * math.sin(dlon / 2))
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
        d = radius * c

        return d

    def get_city_distance(self,city1, city2):
        return self.geo_distance(self.city_info[city1], self.city_info[city2])

    def build_connection(self,city_info):
        cities_connection = defaultdict(list)
        cities = list(city_info.keys())
        for c1 in cities:
            for c2 in cities:
                if c1 == c2: continue

                if self.get_city_distance(c1, c2) < self.threshold:
                    cities_connection[c1].append(c2)
        return cities_connection

    def search_1(self,graph, start, destination):
        pathes = [[start]]  # list 用来存储待搜索路径
        visited = set()  # set用来存储已搜索的节点

        while pathes:
            path = pathes.pop(0)  # 提取第一条路径
            froniter = path[-1]  # 提取即将要探索的节点

            if froniter in visited: continue  # 检查如果该点已经探索过 则不用再探索

            successsors = graph[froniter]

            for city in successsors:  # 遍历子节点
                if city in path: continue  # check loop #检查会不会形成环

                new_path = path + [city]

                pathes.append(new_path)  # bfs     #将新路径加到list里面
                # pathes = [new_path] + pathes #dfs

                if city == destination:  # 检查目的地是不是已经搜索到了
                    return new_path
            visited.add(froniter)


    def search_2(self,graph, start, destination, search_strategy):
        pathes = [[start]]
        visited = set()  # ！
        while pathes:
            path = pathes.pop(0)
            froniter = path[-1]
            if froniter in visited: continue  # ！

            if froniter == destination:  # ！
                return path  # ！

            successsors = graph[froniter]

            for city in successsors:
                if city in path: continue  # check loop

                new_path = path + [city]

                pathes.append(new_path)  # bfs

            pathes = search_strategy(pathes)
            visited.add(froniter)  # ！
        # if pathes and (destination == pathes[0][-1]):
        #     return pathes[0]

    def sort_by_distance(self,pathes):
        def get_distance_of_path(path):
            distance = 0
            for i, _ in enumerate(path[:-1]):
                distance += self.get_city_distance(path[i], path[i + 1])
            return distance

        return sorted(pathes, key=get_distance_of_path)

    def get_distance_of_path(self,path):
        distance = 0
        for i, _ in enumerate(path[:-1]):
            distance += self.get_city_distance(path[i], path[i + 1])
        return distance