
class machine_learning(object):
    def __init__(self):
        pass

    # define loss function
    def loss(self, y, y_hat):
        return sum((y_i - y_hat_i) ** 2 for y_i, y_hat_i in zip(list(y), list(y_hat))) / len(list(y))

    # define partial derivative
    def partial_derivative_k(self, x, y, y_hat):
        n = len(y)
        gradient = 0
        for x_i, y_i, y_hat_i in zip(list(x), list(y), list(y_hat)):
            gradient += (y_i - y_hat_i) * x_i
        return -2 / n * gradient

    def partial_derivative_b(self, y, y_hat):
        n = len(y)
        gradient = 0
        for y_i, y_hat_i in zip(list(y), list(y_hat)):
            gradient += (y_i - y_hat_i)
        return -2 / n * gradient
