#!/usr/bin/env python
# coding: utf-8
import re
from build_graph import build_graph
from sklearn.datasets import load_boston
from machine_learning import machine_learning
import random


#AI for NLP. Lecture 1 Part 1

#Build Graph
print ("=======Build Graph=======")
#城市数据和其经纬度
coordination_source = """
{name:'兰州', geoCoord:[103.73, 36.03]},
{name:'嘉峪关', geoCoord:[98.17, 39.47]},
{name:'西宁', geoCoord:[101.74, 36.56]},
{name:'成都', geoCoord:[104.06, 30.67]},
{name:'石家庄', geoCoord:[114.48, 38.03]},
{name:'拉萨', geoCoord:[102.73, 25.04]},
{name:'贵阳', geoCoord:[106.71, 26.57]},
{name:'武汉', geoCoord:[114.31, 30.52]},
{name:'郑州', geoCoord:[113.65, 34.76]},
{name:'济南', geoCoord:[117, 36.65]},
{name:'南京', geoCoord:[118.78, 32.04]},
{name:'合肥', geoCoord:[117.27, 31.86]},
{name:'杭州', geoCoord:[120.19, 30.26]},
{name:'南昌', geoCoord:[115.89, 28.68]},
{name:'福州', geoCoord:[119.3, 26.08]},
{name:'广州', geoCoord:[113.23, 23.16]},
{name:'长沙', geoCoord:[113, 28.21]},
//{name:'海口', geoCoord:[110.35, 20.02]},
{name:'沈阳', geoCoord:[123.38, 41.8]},
{name:'长春', geoCoord:[125.35, 43.88]},
{name:'哈尔滨', geoCoord:[126.63, 45.75]},
{name:'太原', geoCoord:[112.53, 37.87]},
{name:'西安', geoCoord:[108.95, 34.27]},
//{name:'台湾', geoCoord:[121.30, 25.03]},
{name:'北京', geoCoord:[116.46, 39.92]},
{name:'上海', geoCoord:[121.48, 31.22]},
{name:'重庆', geoCoord:[106.54, 29.59]},
{name:'天津', geoCoord:[117.2, 39.13]},
{name:'呼和浩特', geoCoord:[111.65, 40.82]},
{name:'南宁', geoCoord:[108.33, 22.84]},
//{name:'西藏', geoCoord:[91.11, 29.97]},
{name:'银川', geoCoord:[106.27, 38.47]},
{name:'乌鲁木齐', geoCoord:[87.68, 43.77]},
{name:'香港', geoCoord:[114.17, 22.28]},
{name:'澳门', geoCoord:[113.54, 22.19]}
"""


#Get data from source using regular expression

#regular expression 正则表达式

# [a-z]
# [A-Z]
# [^a]: negation
# colou?r:  ? zero or one of its previous character
# * : zero or more of its previous character
# +: one or more
# .:match any single character
# ^:start of the line
# $:end of the line
# | [cat|dog] : cat or dog
# (da): make the string da like a character
#


l = "color or colour"


pattern = re.compile("colou?r")
pattern.findall(l)

bg = build_graph(coordination_source)


city_info = bg.get_city_info(coordination_source)
bg.city_info = city_info

print (city_info)


# ### Compute distance between cities


print (bg.get_city_distance("杭州","上海"))


#Draw the graph


import networkx as nx
import matplotlib.pyplot as plt
#%matplotlib inline
#plt.rcParams['font.sans-serif'] = ['SimHei']
#plt.rcParams['axes.unicode_minus'] = False



print (city_info.keys())
city_graph = nx.Graph()

city_graph.add_nodes_from(list(city_info.keys()))


nx.draw(city_graph, city_info, with_labels=True, node_size=10)


# ### Build connection between. Let's assume that two cities are connected if their distance is less than 700 km.


bg.threshold = 700   # defined the threshold







cities_connection = bg.build_connection(city_info)


print (cities_connection)


# ### Draw connection graph


cities_connection_graph = nx.Graph(cities_connection)


nx.draw(cities_connection_graph,city_info,with_labels=True,node_size=10)


#BFS 1 version
print (bg.search_1(cities_connection,"上海","香港"))


# ### Optimal search using variation of BFS

print (bg.get_distance_of_path(["北京","济南","上海"]))
print (bg.get_distance_of_path(["北京","天津","上海"]))


print (bg.search_2(cities_connection,"北京","上海",search_strategy=lambda x:x))


print (bg.search_2(cities_connection,"上海","北京",search_strategy=bg.sort_by_distance))


# # AI for NLP Lecture 1 Part 2

#Machine Learning
print ("======Machine Learning======")




dataset = load_boston()
#dataset
x,y=dataset['data'],dataset['target']
print (x.shape)
print (y.shape)
#
#
# print (x[1].shape)
# print (x[1])
# print (dataset.feature_names)
# print (dataset['DESCR'])
X_rm = x[:,5]


# plot the RM with respect to y
plt.scatter(X_rm,y)


# ### Gradient descent
# ### Assume that the target funciton is a linear function
# $$ y = k*rm + b$$

#define target function
def price(rm, k, b):
    return k * rm + b


# ### Define mean square loss

# $$ loss = \frac{1}{n} \sum{(y_i - \hat{y_i})}^2$$

# $$ loss = \frac{1}{n} \sum{(y_i - (kx_i + b_i))}^2 $$


# ### Define partial derivatives

# $$ \frac{\partial{loss}}{\partial{k}} = -\frac{2}{n}\sum(y_i - \hat{y_i})x_i$$
#

# $$ \frac{\partial{loss}}{\partial{b}} = -\frac{2}{n}\sum(y_i - \hat{y_i})$$





#initialized parameters

ml = machine_learning()

k = random.random() * 200 - 100  # -100 100
b = random.random() * 200 - 100  # -100 100

learning_rate = 1e-3

iteration_num = 200
losses = []


for i in range(iteration_num):

    price_use_current_parameters = [price(r, k, b) for r in X_rm]  # \hat{y}

    current_loss = ml.loss(y, price_use_current_parameters)
    losses.append(current_loss)
    print("Iteration {}, the loss is {}, parameters k is {} and b is {}".format(i,current_loss,k,b))

    k_gradient = ml.partial_derivative_k(X_rm, y, price_use_current_parameters)
    b_gradient = ml.partial_derivative_b(y, price_use_current_parameters)

    k = k + (-1 * k_gradient) * learning_rate
    b = b + (-1 * b_gradient) * learning_rate
best_k = k
best_b = b

plt.plot(list(range(iteration_num)),losses)

price_use_best_parameters = [price(r, best_k, best_b) for r in X_rm]

plt.scatter(X_rm,y)
plt.scatter(X_rm,price_use_current_parameters)





