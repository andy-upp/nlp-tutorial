from simple_grammar import  simple_grammar
from data_driven import data_driven
from language_model import language_model

print ("=========1.simplr_grammar=========")
sg = simple_grammar()
a = sg.adj_star()
#print (adj_star())
#  But the question is ?
# 如果我们更换了语法，会发现所有写过的程序，都要重新写。:(

#语法规则
simple_grammar = """
sentence => noun_phrase verb_phrase     
noun_phrase => Article Adj* noun        
Adj* => null | Adj Adj*                 
verb_phrase => verb noun_phrase
Article =>  一个 | 这个
noun =>   女人 |  篮球 | 桌子 | 小猫
verb => 看着   |  坐在 |  听着 | 看见
Adj =>  蓝色的 | 好看的 | 小小的
"""
example_grammar = sg.create_grammar(simple_grammar)
tmp = sg.generate(gram=example_grammar, target='sentence')
print (tmp)

#generate过程
'''
expand=[]
=>1. if sentence not in gram  #首先判断sentence是否在grammer的key当中，如果不在的话 直接返回target
=>2. gram[sentnece] = [['noun_phrase', 'verb_phrase']],  for t in [nount_phrase,verb_phrase]
    generate(gram,noun_phrase)
    ==>1. if noun_phrase not in gram   
    ==>2. gram[noun_phrase]=[['Article', 'Adj*', 'noun']] for t in ['Article', 'Adj*', 'noun']
        generate(gram, Article)
        ==> 1. if Article not in gram
        ==> 2. gram[Article]= [['一个'], ['这个']] 随机选取一个  for t in ["这个"]  t="这个"
          generate(gram,"这个")
          ==>1 if "这个" not  in gram  ==>  return "这个"
       genrate(gram,"Adj*")
       ==> 1. if Adj* not in gram
       ==> 2. gram["Adj*"] = [['null'], ['Adj', 'Adj*']]  t=["noun"]  t="noun"

expand =["一个","好看的" ,"小猫 "]
"一个好看的小猫"
'''

#在西部世界里，一个”人类“的语言可以定义为
human = """
human = 自己 寻找 活动
自己 = 我 | 俺 | 我们 
寻找 = 找找 | 想找点 
活动 = 乐子 | 玩的
"""

#一个“接待员”的语言可以定义为

host = """
host = 寒暄 报数 询问 业务相关 结尾 
报数 = 我是 数字 号 ,
数字 = 单个数字 | 数字 单个数字 
单个数字 = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 
寒暄 = 称谓 打招呼 | 打招呼
称谓 = 人称 ,
人称 = 先生 | 女士 | 小朋友
打招呼 = 你好 | 您好 
询问 = 请问你要 | 您需要
业务相关 = 玩玩 具体业务
玩玩 = null
具体业务 = 喝酒 | 打牌 | 打猎 | 赌博
结尾 = 吗？
"""
#生成20句话
for i in range(20):
    print(sg.generate(gram=sg.create_grammar(host, split='='), target='host'))
#希望能够生成最合理的一句话？


simpel_programming ="""
programming => if_stmt | assign | while_loop
while_loop => while ( cond ) { change_line stmt change_line }
if_stmt => if ( cond )  { change_line stmt change_line } | if ( cond )  { change_line stmt change_line } else { change_line stmt change_line } 
change_line => /N
cond => var op var
op => | == | < | >= | <= 
stmt => assign | if_stmt
assign => var = var
var =>  var _ num | words 
words => words _ word | word 
word => name | info |  student | lib | database 
nums => nums num | num
num => 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0
"""

res = sg.generate(gram=sg.create_grammar(simpel_programming, split='=>'), target='programming')

print("--res--")
print(res)


print ("================2.data_driven================")

generated_programming = []
dd = data_driven()
for i in range(20):
    generated_programming += dd.pretty_print(
        sg.generate(gram=sg.create_grammar(simpel_programming, split='=>'), target='programming'))

for line in generated_programming:
    print(line)




print ("================ 3.language_model ===============")

import jieba
import pandas as pd
from functools import reduce
from operator import add
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter


#random.choice(range(100))

filename = '/Users/liguanghui/Documents/开课吧NLP/第1课/数据集/sqlResult_1558435.csv'
content = pd.read_csv(filename, encoding='gb18030')
#content.head()
articles = content['content'].tolist()
#len(articles)

# 词 ==》中文需要切词 =》 我想要玩游戏 =》 我  想要  玩  游戏 / i want to play games
# jieba
# 我  1
# 想要 1
# 玩 1
# 游戏 1
with_jieba_cut = Counter(jieba.cut(articles[110]))
with_jieba_cut.most_common()[:10]

#''.join(token(articles[110]))

TOKEN = []

reduce(add, [1, 2, 3, 4, 5, 8])

words_count = Counter(TOKEN)
words_count.most_common(100)
TOKEN = [str(t) for t in TOKEN]
TOKEN_2_GRAM = [''.join(TOKEN[i:i + 2]) for i in range(len(TOKEN[:-2]))]

#TOKEN_2_GRAM[:10]

words_count_2 = Counter(TOKEN_2_GRAM)


lm = language_model(words_count,TOKEN,words_count_2,TOKEN_2_GRAM)
articles_clean = [''.join(lm.token(str(a))) for a in articles]

# len(articles_clean)

with open('article_9k.txt', 'w') as f:
    for a in articles_clean:
        f.write(a + '\n')




for i, line in enumerate((open('article_9k.txt'))):
    if i % 100 == 0: print(i)

    # replace 10000 with a big number when you do your homework.

    if i > 10000: break
    TOKEN += lm.cut(line)





frequiences = [f for w, f in words_count.most_common(100)]

x = [i for i in range(100)]

#get_ipython().run_line_magic('matplotlib', 'inline')


plt.plot(x, frequiences)
plt.plot(x, np.log(frequiences))


# 计算单个词出现的概论
# def prob_1(word,):
#     return words_count[word] / len(TOKEN)


# count(wk)/(number of words)

# 两个词同时出现的概论
# p(w1|w2) = count(w1,w2)/count（w1)


#lm.prob_1('我们',words_count,TOKEN)
#TOKEN[:10]




# def prob_1(word):
#     return words_count[word] / len(TOKEN)
#
#
# # 两个词同时出现的概论
# def prob_2(word1, word2):  # p(w1,w2) = count(w1,2)/count(w1)
#     if word1 + word2 in words_count_2:
#         return words_count_2[word1 + word2] / words_count[word1]
#     else:
#         return 1 / len(TOKEN_2_GRAM)


#  (w1 w2), (w3,w4) (w4,w5)  2-gram
# (w1,w3)  1/3


lm.prob_2('我们', '在')
lm.prob_2('在', '吃饭')
lm.prob_2('去', '吃饭')


# def get_probablity(sentence):
#     words = cut(sentence)
#
#     sentence_pro = 1
#
#     for i, word in enumerate(words[:-1]):
#         next_ = words[i + 1]
#
#         probability = prob_2(word, next_)  # p(w1|w2)
#
#         sentence_pro *= probability  # p(s) = p(w_1)p(w2|w1)*p(w3|w2)..p(wn|wn-1)
#
#     return sentence_pro


lm.get_probablity('小明今天抽奖抽到一台苹果手机')
lm.get_probablity('小明今天抽奖抽到一架波音飞机')
lm.get_probablity('洋葱奶昔来一杯')
lm.get_probablity('养乐多绿来一杯')

for sen in [sg.generate(gram=example_grammar, target='sentence') for i in range(10)]:
    print('sentence: {} with Prb: {}'.format(sen, lm.get_probablity(sen)))

need_compared = [
    "今天晚上请你吃大餐，我们一起吃日料 明天晚上请你吃大餐，我们一起吃苹果",
    "真事一只好看的小猫 真是一只好看的小猫",
    "今晚我去吃火锅 今晚火锅去吃我",
    "洋葱奶昔来一杯 养乐多绿来一杯"
]

for s in need_compared:
    s1, s2 = s.split()
    p1, p2 = lm.get_probablity(s1), lm.get_probablity(s2)

    better = s1 if p1 > p2 else s2

    print('{} is more possible'.format(better))
    print('-' * 4 + ' {} with probility {}'.format(s1, p1))
    print('-' * 4 + ' {} with probility {}'.format(s2, p2))
