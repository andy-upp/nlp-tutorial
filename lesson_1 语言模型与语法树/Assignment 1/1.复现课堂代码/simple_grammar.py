#!/usr/bin/env python
# coding: utf-8

import random



class simple_grammar(object):

    def adj(self):
        #choice() 方法返回一个列表，元组或字符串的随机项,此处是列表
        return random.choice('蓝色的 | 好看的 | 小小的'.split('|')).split()[0]

    def adj_star(self):
        #选择一个adj，在选一个adj_star()，递归选取若干个,  或者直接返回一个空
        return random.choice([lambda : '', lambda : self.adj() + self.adj_star()])()

    # 把形容词变成一个规范的结构，形成一个dictionary
    def create_grammar(self,grammar_str, split='=>', line_split='\n'):
        grammar = {}
        for line in grammar_str.split(line_split):
            # line.strip()返回移除字符串头尾指定字符生成的新字符串，此处是移除空格
            if not line.strip(): continue
            exp, stmt = line.split(split)
            grammar[exp.strip()] = [s.split() for s in stmt.split('|')]
        return grammar

    # grammar = create_grammar(adj_grammar)
    def generate(self,gram, target):
        choice = random.choice
        if target not in gram:
            return target  # means target is a terminal expression #1
        expaned = [self.generate(gram, t) for t in choice(gram[target])]  # 2 递归检索字典树
        # 将expaned中的词拼接起来，如果e不等于下一行或者不等于其它的符号或者不等于'null'，就把这些词拼成一句话
        return ''.join([e if e != '/n' else '\n' for e in expaned if e != 'null'])











