#!/usr/bin/env python
# coding: utf-8

# # Language Model
##$$ language\_model(String) = Probability(String) \in (0, 1) $$
# $$ Pro(w_1 w_2 w_3 w_4) = Pr(w_1 | w_2 w_3 w_ 4) * P(w2 | w_3 w_4) * Pr(w_3 | w_4) * Pr(w_4)$$
# $$ Pro(w_1 w_2 w_3 w_4) \sim Pr(w_1 | w_2 ) * P(w2 | w_3 ) * Pr(w_3 | w_4) * Pr(w_4)$$
# how to get $ Pr(w1 | w2 w3 w4) $ ?

import re
import jieba



class language_model(object):

    def __init__(self,words_count,TOKEN,words_count_2,TOKEN_2_GRAM):
        self.words_count = words_count
        self.TOKEN = TOKEN
        self.words_count_2 = words_count_2
        self.words_count = words_count
        self.TOKEN_2_GRAM = TOKEN_2_GRAM


    def token(self,string):
        # we will learn the regular expression next course.
        return re.findall('\w+', string)

    def cut(self,string):
        return list(jieba.cut(string))

    # 计算单个词出现的概论
    def prob_1(self,word):
        return self.words_count[word] / len(self.TOKEN)

    # 两个词同时出现的概论
    def prob_2(self,word1, word2):  # p(w1,w2) = count(w1,2)/count(w1)
        if word1 + word2 in self.words_count_2:
            return self.words_count_2[word1 + word2] / self.words_count[word1]
        else:
            return 1 / len(self.TOKEN_2_GRAM)

    def get_probablity(self,sentence):
        words = self.cut(sentence)

        sentence_pro = 1

        for i, word in enumerate(words[:-1]):
            next_ = words[i + 1]

            probability = self.prob_2(word, next_)  # p(w1|w2)

            sentence_pro *= probability  # p(s) = p(w_1)p(w2|w1)*p(w3|w2)..p(wn|wn-1)

        return sentence_pro