#!/usr/bin/env python
# coding: utf-8

#Data Driven

# 我们的目标是，希望能做一个程序，然后，当输入的数据变化的时候，我们的程序不用重写。Generalization.
# AI? 如何能自动化解决问题，我们找到一个方法之后，输入变了，我们的这个方法，不用变。



class data_driven(object):
    def __init__(self):
        pass


    def pretty_print(self,line):
        # utility tool function
        lines = line.split('/N')
        code_lines = []

        for i, sen in enumerate(lines):
            if i < len(lines) / 2:
                # print()
                code_lines.append(i * "  " + sen)
            else:
                code_lines.append((len(lines) - i) * " " + sen)

        return code_lines




