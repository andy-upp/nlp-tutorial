import numpy as np

#基类
class ModelBase(object):
    def __init__(self, *args, **kwargs):
        self.params = {} #存储当前层的参数
        self.grads = {} #当前参数的梯度
        self.results = {} #中间计算结果

    #前向传播
    def forward(self,*args, **kwargs):
        pass

    #反向传播
    def backward(self,*args, **kwargs):
        pass


#线性层
class Linear(ModelBase):
    #初始化函数形成线性层的一个基类
    '''
    input_dim: 输入维度
    output_dim: 输出维度
    bias: 是否使用偏置项
    '''
    def __init__(self, input_dim, output_dim, bias = True):
        super(Linear,self).__init__() #使用父类初始化
        #注册参数
        self.bias = bias
        self.W = np.random.randn(input_dim,output_dim)/np.sqrt(input_dim)
        self.params['weight'] = self.W
        if bias:
            self.b = np.zeros(output_dim)
            self.params['bias'] = self.b

    def forward(self, x):
        #check type
        assert isinstance(x,np.ndarray)
        # x: batch_size x input_dim
        batch_size = x.shape[0]
        y = np.dot(x,self.W)
        if self.bias:
            y += np.tile(self.b, (batch_size,1),)
        self.results['x'] = x
        return y

    def backward(self, grad):
        #check type
        assert isinstance(grad, np.ndarray)
        #grad: batch_size x output_dim
        grad_input = np.dot(grad,self.W.T)#batch_size x input_dim
        #gradient of such layer
        x = self.results['x'] #batch_size x input_dim
        batch_size = x.shape[0]
        input_dim = x.shape[1]
        output_dim = grad.shape[1]
        x_unsqueeze = x.reshape((batch_size,input_dim,1))
        ...

class Sigmoid(ModelBase):
    def __init__(self):
        pass

    def forward(self,x):
        pass

    def backward(self, grad):
        pass


class Tanh(ModelBase):
    def __init__(self):
        pass

    def forward(self,x):
        pass

    def backward(self, grad):
        pass


class RELU(ModelBase):
    def __init__(self):
        pass

    def forward(self, x):
        pass

    def backward(self, grad):
        pass


class L2Loss(ModelBase):
    def __init__(self):
        pass

    def forward(self,x,groud_truth):
        pass

    def backward(self):
        pass


class BinaryCrossEntropyLoss(ModelBase):
    def __init__(self):
        pass

    def forward(self,x,label):
        pass

    def backward(self):
        pass





















































