import pinyin
import re
from collections import Counter, defaultdict



class pinyin_auto_correction_problem(object):
    def __init__(self):
        self.alphabet = {}
        self.PINYIN_COUNT = {}

    #将汉字转为拼音
    def chinese_to_pinyin(self,character):
        return pinyin.get(character, format='strip', delimiter=' ')

    #只找汉字的拼音，忽略数字
    def tokens(self,text):
        "List all the pinyin characters"
        return re.findall('[a-z]+', text.lower())

    def correct(self,word):  # pinyin candidates piyin pingyin aogyin ....
        'Find the most possible pinyin based on edit distance'
        # Prefer edit distance 0, then 1, then 2; otherwist default to word itself
        candidates = (self.known(self.edits0(word)) or
                      self.known(self.edits1(word,self.alphabet)) or
                      self.known(self.edits2(word)) or
                      [word])  # 计算所有与输入词编辑距离为0，1，2的词
        return max(candidates, key=self.PINYIN_COUNT.get)  # 取出在所有candidate里在语料库里出现次数最多的词
    #得出拼音里的所有词
    def known(self,words):
        'Return the pinyin in our data'
        return {w for w in words if w in self.PINYIN_COUNT}

    #得出编辑距离为0的词，就是这个词本身
    def edits0(self,word):
        'Return all strings that are zero edits away from word (i.e., just word itself).'
        return {word}

    def edits2(self,word):
        'Return all strings that are two edits away from this pinyin.'
        return {e2 for e1 in self.edits1(word,self.alphabet) for e2 in self.edits1(e1,self.alphabet)}  # 要计算编辑距离为2的词 可以通过计算与原词编辑距离为1的词的编辑距离为1的词

    def splits(self,word):
        'Return a list of all possible (first, rest) pairs that comprise pinyin.'
        return [(word[:i], word[i:])
                for i in range(len(word) + 1)]

    def edits1(self,word,alphabet):
        'Return all strings that are one edit away from this pinyin.'
        pairs = self.splits(word)
        deletes = [a + b[1:] for (a, b) in pairs if b]  # input pinyin -删除操作 去掉b[0]
        replaces = [a + c + b[1:] for (a, b) in pairs for c in alphabet if b]  # 替换  b[0]替换为c
        inserts = [a + c + b for (a, b) in pairs for c in alphabet]  # 插入操作 在a和b之间插入字母
        return set(deletes + replaces + inserts)

    # map(f,[a,b,v]) = f(a),f(b),f(v)
    def correct_sequence_pinyin(self,text_pinyin):
        return ' '.join(map(self.correct, text_pinyin.split()))
