#!/usr/bin/env python
# coding: utf-8
from collections import defaultdict
from solve_cut_rod_probelm import solve_cut_rod_probelm
import how_to_optimize
from edit_distance import edit_distance
from pinyin_auto_correction_problem import pinyin_auto_correction_problem
from collections import Counter, defaultdict
import pinyin

#solve cut rod probelm
print ("================ 1.solve_cut_rod_probelm ===============")
#价格表 分别代表1到11米的价格
original_prica = [1, 5, 8, 9, 10, 17, 17, 20, 24, 30, 35]

#defaultdict的作用是在于，当字典里的key不存在但被查找时，返回的不是keyError而是一个默认值
price = defaultdict(int)

#enumerate() 函数用于将一个可遍历的数据对象(如列表、元组或字符串)组合为一个索引序列，
# 同时列出数据和数据下标，一般用在 for 循环当中。
for i, p in enumerate(original_prica):
    #i从0开始
    price[i + 1] = p

scrd = solve_cut_rod_probelm(price)

pr = scrd.r(10)
print (pr)

print ("==========2.Analysis: How to optimize===========")

'''
import time

# @get_time
def fibonacci(n):
    if n <= 2:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


start = time.time()
print(fibonacci(34))
end = time.time()
print(end - start)

# In[241]:


mem = defaultdict()


def fibonacci_op(n):
    if n in mem:
        return mem[n]
    else:
        if n <= 2:
            mem[n] = 1
            return n
        else:
            result = fibonacci_op(n - 1) + fibonacci_op(n - 2)
            mem[n] = result
            return result


# In[242]:


start = time.time()
print(fibonacci_op(64))
end = time.time()
print(end - start)
'''

hto = how_to_optimize.how_to_optimize(price)
hto.g("hello")
#由于该函数是在wrapper函数中执行的，所以返回的是wrapper函数的name和注解
#如果使用这个装饰器的话 别人想看example的name和注解就看不到了
print(hto.example.__name__, hto.example.__doc__)


#可以使用python自带的wraps注解解决这个问题
print(hto.example_.__name__, hto.example_.__doc__)



# wraps 函数作用
# # We use this method to solve Cut Rod probelm¶
# 使用memo的调用过程
# =》memo(r) =>进入memo 初始化already_computed => return _wrap => 进入 _wrap  => 判断 arg  在already_computed 返回数值 =》不在 调用 f函数 这里f=r 即调用r函数计算 


# max_price = hto.r(15)
# print(max_price)


from functools import wraps
def memo(f):
    already_computed = {} #用来存储计算过的数值
    @wraps(f)
    def _wrap(arg):
        if arg in already_computed:  #检测是否之前已经算过
            result = already_computed[arg] #如果算过直接返回数值
        else:
            result = f(arg)              #如果没有算过 调用r函数来计算数值
            already_computed[arg] = result #把结果存储到dic里面以供后面使用
        return result
    return _wrap


solution = {}

#此处给成员函数加装饰器的话 得不到正确结果 原因不明
@memo
def r(n):
    """
    Args: n is the iron length
    Return: the max revenue
    """
    max_price, max_split = max(
        [(price[n], 0)] + [(r(i) + r(n - i), i) for i in range(1, n)], key=lambda x: x[0]
    )

    solution[n] = (n - max_split, max_split)
    return max_price

print (r(15))


#How do we parse solution?

'''
def parse_solution(n):
    left_split, right_split = solution[n]

    if right_split == 0: return [left_split]

    return parse_solution(left_split) + parse_solution(right_split)

parse_solution(20)

'''

print ("==============Edit Distance=============")
# Edit Distance
ed = edit_distance()
print(ed.edit_distance('ABCDECG', 'ABCCEF'))


# Problem Case 3: Pinyin Auto Correction Problem
print ("===========Pinyin Auto Correction Problem===========")
#语料库
chinese_dataset = 'article_9k.txt'

CHINESE_CHARATERS = open(chinese_dataset).read()

print (CHINESE_CHARATERS[:40])
print (pinyin.get('你好，中国', format='strip', delimiter=' '))



pacp =  pinyin_auto_correction_problem()

CHINESE_CHARATERS_COPYS = pacp.chinese_to_pinyin(CHINESE_CHARATERS)

# print (len(CHINESE_CHARATERS_COPYS))
#
#
# print (CHINESE_CHARATERS_COPYS[:100])
#
#
# print (pacp.tokens(CHINESE_CHARATERS_COPYS[:100]))


PINYIN_COUNT = Counter(pacp.tokens(CHINESE_CHARATERS_COPYS))


alphabet = 'abcdefghijklmnopqrstuvwxyz'

pacp.alphabet = alphabet
pacp.PINYIN_COUNT = PINYIN_COUNT

# input pinyin
# deltes = [inyin, pnyin,piyin,pinin,pinyi,pinyin]
# replces = [ainyin,binyin,cinyin,.....,panyin,pbnyin,....]
# inserts = [apinyin,bpinyin,cpinyin,....,painyin,pbainyin,...]



print (pacp.splits('pinyin'))

#编辑距离为0的词
print(pacp.edits0('pinyin'))
#编辑距离为1的词
print(pacp.edits1('pinyin',alphabet))



# # Test

print (pacp.correct('yin'))

print (pacp.correct('yign'))

print (pacp.correct('yinn'))




#pacp.correct_sequence_pinyin('zhe sih yi ge ce sho')

print (pacp.correct_sequence_pinyin('wo xiang shagn qinng hua da xue'))

# # 思考题-homework？    
# #### 如何在不带空格的时候完成自动修整？--> 如何完成拼音的自动分割？   
# ###### 提示：使用第一节课提到的语言模型!

# woyaoshangqinghua
# w yaoshangqinghua
# wo yaoshangqinghua
# woyao shangqinghua
# 
# -> DP
