from collections import defaultdict



class solve_cut_rod_probelm:

    def __init__(self,price):
        self.price = price


    def r(self,n):  # 返回最大利润
        #max() 方法返回给定参数的最大值，参数可以为序列,此处是返回不切和所有切法利润较大的那个
        return max(
            # 不切的利润         递归遍历所有切法的利润
            [self.price[n]] + [self.r(int(i)) + self.r(int(n - i)) for i in range(1,int((n+1)/2))]
        )

    # 不太懂递归的同学 可以用下面函数看递归过程
    def studey_r(self,n):  # 返回最大利润

        cuts = [n]
        cuts += [(i, n - i) for i in range(1, n // 2 + 1)]
        l = [self.price[n]] + [self.studey_r(i) + self.studey_r(n - i) for i in range(1, n // 2 + 1)]
        # 不切的利润     遍历所有切法的利润
        best = max(l)
        i = l.index(best)
        print("当 n={}, 有这些切法:{},对应的价格为:{},最好的切法为价格最高的即:{}".format(n, cuts, l, ([cuts[i]], best)))
        return best








#if __name__ == '__main__':
#    print("test")
