import time
from functools import wraps

'''
功能：这个装饰器告诉我们一个函数使用的时间
描述：这个装饰器接收一个函数作为参数，同时内嵌一个函数，在内嵌的函数中
     调用传入的函数，计算该函数执行所用的时间。
     返回值就是内嵌函数，当函数被装饰器修饰时，实际上是在内嵌函数中调用该函数
'''
def get_time(func):
    def wrapper(*args):
        start = time.time()
        func(*args)  # 调用传入的func
        end = time.time()
        print('used time : {}'.format(end - start))  # func这个函数花费的时间

    return wrapper

'''
f1(func=g) -> return wrapper ->print start -> run g print(hello) -> print end 
'''
def f1(func):
    def wrapper(*args, **kwargs):
        print('Started')
        func(*args, **kwargs)
        print('Ended')

    return wrapper


def my_decorator(func):
    def wrapper(*args, **kwargs):
        """decorator"""
        print('Calling decorated function...')
        return func(*args, **kwargs)

    return wrapper

#在wrapper前使用wraps装饰器可以得到被装饰函数本身的name和注解
def my_decorator_(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print('Calling decorated function...')
        return func(*args, **kwargs)

    return wrapper

#memo装饰器，参数为计算价格时的函数
def memo(f):
    already_computed = {}  # 用来存储计算过的数值

    @wraps(f)
    def _wrap(self,arg):
        if arg in already_computed:  # 检测是否之前已经算过
            result = already_computed[arg]  # 如果算过直接返回数值
        else:
            result = f(self,arg)  # 如果没有算过 调用r函数来计算数值
            already_computed[arg] = result  # 把结果存储到dic里面以供后面使用
        return result

    return _wrap




class how_to_optimize(object):
    def __init__(self,price):
        self.price = price
        self.solution = {}

    @f1
    def g(self,a):
        print(a)

    @my_decorator
    def example(self):
        """Docstring"""
        print('Called example function')

    @my_decorator_
    def example_(self):
        """Docstring"""
        print('Called example function')

    @memo
    def r(self,n):
        """
        Args: n is the iron length
        Return: the max revenue
        """

        print ("nnnnnnn",n)
        max_price, max_split = max(
            [(self.price[n], 0)] + [(self.r(i) + self.r(n - i), i) for i in range(1, n)], key=lambda x: x[0]
        )

        self.solution[n] = (n - max_split, max_split)
        print ("max_price",max_price)
        return max_price


if __name__ == '__main__':
    from collections import defaultdict
    original_prica = [1, 5, 8, 9, 10, 17, 17, 20, 24, 30, 35]

    price = defaultdict(int)
    hto = how_to_optimize(price)
    print (hto.r(15))
