from functools import lru_cache
from functools import wraps


# def lru_cache(f):
#     already_computed = {}  # 用来存储计算过的数值
#
#     @wraps(f)
#     def _wrap(self,arg):
#         if arg in already_computed:  # 检测是否之前已经算过
#             result = already_computed[arg]  # 如果算过直接返回数值
#         else:
#             result = f(self,arg)  # 如果没有算过 调用r函数来计算数值
#             already_computed[arg] = result  # 把结果存储到dic里面以供后面使用
#         return result
#
#     return _wrap


class edit_distance(object):
    def __init__(self):
       self.solution = {}

    #lru_cache是python自带的装饰器，存储已经计算过的数据，maxsize表示最多存储多少数据
    @lru_cache(maxsize=2 ** 10)
    def edit_distance(self,string1, string2):
        if len(string1) == 0: return len(string2)  # 如果词1是空白 那么距离为词2的长度
        if len(string2) == 0: return len(string1)  # 如果词2为空白，那么编辑距离为词1的距离

        tail_s1 = string1[-1]
        tail_s2 = string2[-1]

        # 迭代公式 D[i,j] = min(D[i-1,j]+1, D[i,j-1]+1, D[i-1,j-1]+1/0)
        candidates = [  # 计算D[i-1,j]+1的距离            删除操作
            (self.edit_distance(string1[:-1], string2) + 1, 'DEL {}'.format(tail_s1)),
            # string 1 delete tail
            # 计算D[i,j-1]+1的距离   插入操作
            (self.edit_distance(string1, string2[:-1]) + 1, 'ADD {}'.format(tail_s2)),
            # string 1 add tail of string2
        ]
        #如果尾部加入的词是一样的，则不需要做任何替换操作，直接加0
        if tail_s1 == tail_s2:  # D[i-1,j-1]+0   D[i-1,j-1]=D[bc,bce] ==> D[i,j]=D[bcl,bcek]
            both_forward = (self.edit_distance(string1[:-1], string2[:-1]) + 0, '')
        else:  # D[i-1,j-1]+1  如果不一样的话 就要把一个词变成另一个词，有一个替换操作，尾部加1
            both_forward = (self.edit_distance(string1[:-1], string2[:-1]) + 1, 'SUB {} => {}'.format(tail_s1, tail_s2))

        candidates.append(both_forward)
        #选出3钟操作最小的编辑距离，operation是所做的操作
        min_distance, operation = min(candidates, key=lambda x: x[0])  # min（D[i-1,j]+1, D[i,j-1]+1,D[i-1,j-1]+1/0）

        self.solution[(string1, string2)] = operation  # 记录在对应的字符串对里该使用的操作

        return min_distance

